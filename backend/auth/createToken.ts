const SECRET_KEY = process.env.SECRET_KEY

const createToken = (email: string, password: string) => {
    const salt = (Math.floor(Math.random() * 90000) + 10000)
    const unhashedToken = email + ':' + password + ':' + salt + ':' + SECRET_KEY

    const hash = Buffer.from(unhashedToken).toString('base64')

    return hash
}

export default createToken;