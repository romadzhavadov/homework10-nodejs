import { Request, Response } from 'express';
import { DatabaseService } from '../database.service' 
import hashPassword from '../auth/hashPassword';
import createToken from '../auth/createToken';
import { Strategy } from 'passport-http-bearer';
import passport from 'passport';


export class BaseRepository {
 
    constructor( ) {
        this.addNews = this.addNews.bind(this);
        this.getItems = this.getItems.bind(this);
        this.getOneItems = this.getOneItems.bind(this);
        this.updateNews = this.updateNews.bind(this);
        this.deleteNews = this.deleteNews.bind(this);
        this.registerUser = this.registerUser.bind(this);
        this.loginUser = this.loginUser.bind(this);
    }

    async getItems (table: string, req: Request, res: Response) {

        const page = Number(req.query.page) || 1
        const size = Number(req.query.size) || 4
 
        const data = await DatabaseService.readAll(table, 
            {
                page,
                size,
            });
        return data
    }
    
    async getOneItems (table: string, req: Request, res: Response) {
        const data = await DatabaseService.read(table, Number(req.params.id));
        return data
    }
    
    async addNews (table: string, req: Request, res: Response) {
        const data = await DatabaseService.create(table, req.body);
        return data
    }
    
    async updateNews (table: string, req: Request, res: Response) {
        const data = await DatabaseService.read(table, Number(req.params.id));
        if (!data) {
            return { errorMessage: "News not found" };
        }
        const updated = await DatabaseService.update(table, Number(req.params.id), req.body);
        const updateData = await DatabaseService.read(table, Number(req.params.id));
        return updateData
    }
    
    async deleteNews (table: string, req: Request, res: Response) {
        const data = await DatabaseService.read(table, Number(req.params.id));
        if (!data) {
            return { errorMessage: "News not found" };
        }

        await DatabaseService.delete(table, Number(req.params.id));
        return { message: `${table} was  removed` }
    }

    async registerUser(table: string, req: Request, res: Response) {
        // Валідація даних користувача
        const { email, password, confirmPassword } = req.body;

        // Перевірка, чи співпадають паролі
        if (password !== confirmPassword) {
            return { errorMessage: "Passwords do not match" };
        }

        if (email) {
            const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
            if (!emailRegex.test(email)) {
                return { errorMessage: "Invalid email format" };
            }
        }

        const hashedPassword = hashPassword(password)
        const data = {
            email: email,
            password: hashedPassword
        }

        const user = await DatabaseService.registerUser(table, data);
        if (user) {
            return createToken(email, password)
        }
    }

    async loginUser(table: string, req: Request, res: Response) {
        const { email, password } = req.body;

        const user = await DatabaseService.getUser(table, email);

        if (user.password === hashPassword(password)) {
            return createToken(email, password)
        }
    }

    static async getUserByEmail(email: string) {
        const user = await DatabaseService.getUser('users', email);
        return user
    }

    async getAuthUser(table: string, req: Request, res: Response) {
        const { email, password } = req.body;

        const user = await DatabaseService.getUser(table, email);
        return user
    }
}
