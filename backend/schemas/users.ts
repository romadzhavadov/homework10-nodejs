export default {
    name: 'users',
    fields: [
        {
            name: 'id',
            type: 'increments',
            positive: true,
            integer: true
        },
        {
            name: 'email',
            type: 'string',
            max: 50
        },
        {
            name: 'password',
            type: 'string',
            max: 50
        },
    ]
}