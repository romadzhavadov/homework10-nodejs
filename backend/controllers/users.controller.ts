import { BaseController } from './base.controller';
import { UsersRepository } from '../repository.service/users.repository.service'; 

class UsersController extends BaseController {
  constructor() {
    const usersRepository = new UsersRepository()
    super('users', usersRepository);
  }
  
}

export default UsersController;

