import { BaseController } from './base.controller';
import { NewsRepository } from '../repository.service';

class NewsController extends BaseController {
  constructor() {
    const newsRepository = new NewsRepository()
    super('newsposts', newsRepository);
  }
  
}

export default NewsController;

