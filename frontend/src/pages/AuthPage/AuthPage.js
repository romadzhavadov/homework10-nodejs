import React, { useState } from 'react';
import { useNavigate, Link } from 'react-router-dom';
import styles from './AuthPage.module.scss';
import axios from 'axios';


const AuthPage = ( { action }) => {

  const navigate = useNavigate();


  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [confirmPassword, setConfirmPassword] = useState(''); 
  const [err, setErr] = useState(null);


  const registerSubmit = async (e) => {
    e.preventDefault();

    const registerForm = {
      email: email,
      password: password,
      confirmPassword: confirmPassword,
    };

    const loginForm = {
      email: email,
      password: password,
    };

    console.log(loginForm)

    try {
      if (action === "register") {
        const data = await axios.post(`http://localhost:8000/auth/register`, registerForm);
        console.log(data)
      } else {
        const data = await axios.post(`http://localhost:8000/auth/login`, loginForm);
        console.log(data)
      }

      navigate('/');
    } catch (error) {
      console.error('Error auth:', error);
      console.log(error.response.data)
      setErr(error.response ? error.response.data : "Unknown error")
    } 
  };

  return (
    <div className={styles.container}>
      <h2>{action === "register" ? 'Реєстрація Користувача:' : 'Введіть Ваші дані:'}</h2>
      <form onSubmit={registerSubmit}>

        <div className={styles.wrap}>
          <label className={styles.item}>Email:</label>
          <input className={styles.item} type="email" value={email} onChange={(e) => setEmail(e.target.value)}/>
        </div>
        <div className={styles.wrap}>
          <label className={styles.item}>Пароль:</label>
          <input className={styles.item} type="text" value={password} onChange={(e) => setPassword(e.target.value)}/>
        </div>
        {action === "register" ? 
          <div className={styles.wrap}>
            <label className={styles.item}>Повторити Пароль:</label>
            <input className={styles.item} type="text" value={confirmPassword} onChange={(e) => setConfirmPassword(e.target.value)}/>
          </div> 
          : null
        }
        <div className={styles.btnGroup}>
          <button type="submit">Зберегти</button>
          <button onClick={() => navigate('/')}>Повернутися до списку новин</button>
        </div>
      </form>
      {err ? alert(`${err.error} Try again`) : null}
      {action === "register" ? 
          <Link to={`/login`}  >
            <p className={styles.info}> Вже зареєстрований?</p>
          </Link> 
          : null
        }
    </div>
  );
};

export default AuthPage;