import styles from './Header.module.scss';
import Nav from "../Nav";
import { Link } from 'react-router-dom';

const Header = () => {
    return (
        <header className={styles.header}>
            <span className={styles.logo}>Logo</span>
            <Nav />

          <div className={styles.quantityWrapper}>
            <Link to={`/edit`}>
              <button className={styles.btn}>Add News</button>
            </Link>
            <Link to={`/auth`}>
              <button className={styles.btn}>Auth</button>
            </Link>
          </div>
        </header>
    )
}

export default Header;